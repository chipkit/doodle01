/*! \file test_PWM.pde
 *
 *  \brief Exercise pulse width modulation on OC1
 *
 *
 *  \author jjmcd
 *  \date 2102-12-28
 *
 */

/* I/O Shield OLED classes */
#include <IOShieldOled.h>

/* Pin definitions for the Basic I/O Shield */
#include "../basic_io_board.h"

/* Create an instance of the OLED class */
extern IOShieldOledClass oled;

/*! Modulate a LED on OC1 with PWM while displaying duty cycle */
void testPWM( void )
{
  char szWork[16];
  int i,j;

  oled.clear();
  oled.setCursor(1,0);
  oled.putString("P W M   T e s t");
  oled.setCursor(4,2);
  oled.putString("PWM Value");
  for ( j=0; j<4; j++ )
  {
    for ( i=0; i<254; i++ )
    {
      oled.setCursor(6,3);
      sprintf(szWork,"%4.1f%%",100.0*(float)i/255.0);
      oled.putString(szWork);
      analogWrite( PIN_IOOC1, i );
      delay(1);
    }
    for ( i=253; i>=0; i-- )
    {
      oled.setCursor(6,3);
      sprintf(szWork,"%4.1f%%",100.0*(float)i/255.0);
      oled.putString(szWork);
      analogWrite( PIN_IOOC1, i );
      delay(1);
    }
  }
}


