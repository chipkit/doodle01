/*! \file test_TCN75A.pde
 *
 *  \brief Read temperature sensor and display results
 *
 *  \author jjmcd
 *  \date 2012-12-28
 */

#include <Wire.h>
#include <IOShieldOled.h>
#include <IOShieldTemp.h>

/* Create an instance of the OLED class */
extern IOShieldOledClass oled;

/*! Read the TCN75A temperature sensor and display result */
void testTCN75A( void )
{
  float fTemp;
  char szWork[16];
  int i;
  
  IOShieldTemp.config(IOSHIELDTEMP_ONESHOT | IOSHIELDTEMP_RES11 | IOSHIELDTEMP_ALERTHIGH);
  oled.clear();
  oled.setCursor(1,0);
  oled.putString("TCN75A Digital");
  oled.setCursor(2,1);
  oled.putString("Thermometer");
  for ( i=0; i<16; i++ )
  {
  digitalWrite(PIN_LED1,LOW);
  digitalWrite(PIN_LED2,HIGH);
  fTemp = IOShieldTemp.getTemp();
  sprintf(szWork,"%4.1fC %4.1fF",fTemp,
    IOShieldTemp.convCtoF(fTemp));
  oled.setCursor(2,2);
  oled.putString(szWork);
  digitalWrite(PIN_LED1,LOW);
  digitalWrite(PIN_LED2,LOW);
  delay(250);
  }
}

