/*! \file buttons_switches.pde
 *
 *  \brief Read buttons and switches and display results
 *
 *  \author jjmcd
 *  \date 2012-12-26
 */

/* I/O Shield OLED classes */
#include <IOShieldOled.h>

/* Create an instance of the OLED class */
extern IOShieldOledClass oled;



/*! Show the position of the slide switches on the OLED */
void showSwitches( void )
{
  /* Clear the display */
  oled.clear();

  /* Show the posiiton of switch 1 on line 1 of the display */
  oled.setCursor(0,0);
  oled.putString("Switch 1 ");
  if ( digitalRead(PIN_IOSW1) )
    oled.putString("up");
  else
    oled.putString("down");

  /* And similarly for the other switches */
  oled.setCursor(0,1);
  oled.putString("Switch 2 ");
  if ( digitalRead(PIN_IOSW2) )
    oled.putString("up");
  else
    oled.putString("down");

  oled.setCursor(0,2);
  oled.putString("Switch 3 ");
  if ( digitalRead(PIN_IOSW3) )
    oled.putString("up");
  else
    oled.putString("down");

  oled.setCursor(0,3);
  oled.putString("Switch 4 ");
  if ( digitalRead(PIN_IOSW4) )
    oled.putString("up");
  else
    oled.putString("down");
}

/*! Show the state of the pushbuttons on the OLED */
void showButtons( void )
{
  /* Clear the display */
  oled.clear();

  /* Show the state of button 1 on line 1 */
  oled.setCursor(0,0);
  oled.putString("Button1 ");
  if ( digitalRead(PIN_IOBTN1) )
    oled.putString("pressed");
  else
    oled.putString("released");

  /* And likewise ... */
  oled.setCursor(0,1);
  oled.putString("Button2 ");
  if ( digitalRead(PIN_IOBTN2) )
    oled.putString("pressed");
  else
    oled.putString("released");

  oled.setCursor(0,2);
  oled.putString("Button3 ");
  if ( digitalRead(PIN_IOBTN3) )
    oled.putString("pressed");
  else
    oled.putString("released");

  oled.setCursor(0,3);
  oled.putString("Button4 ");
  if ( digitalRead(PIN_IOBTN4) )
    oled.putString("pressed");
  else
    oled.putString("released");
}


