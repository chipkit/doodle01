/*! \file doodle01.pde
 *
 *  \brief Simple exercise of the I/O shield
 *
 *  This is a dumb little exercise to get an understanding of the
 *  Basic I/O Shield.  The OLED display is manipulated a bit using
 *  the Digilent library, and a bunch of LEDs are flashed.
 *
 *  The stupid name of this file is simply the default provided
 *  by MPIDE.
 *
 *  \author jjmcd
 *  \date 2102-12-26
 *
 */
 
/*! I/O Shield OLED classes */
#include <IOShieldOled.h>

/*! Pin definitions for the Basic I/O Shield */
#include "../basic_io_board.h"

/*! Create an instance of the OLED class */
IOShieldOledClass oled;

/* Functions in other files */
/*! Show four lines of text on the OLED */
void showTextLines( void );
/*! Show four lines of text on the OLED using graphical characters */
void showGraphicLines( void );
/*! Show the position of the slide switches on the OLED */
void showSwitches( void );
/*! Show the state of the pushbuttons on the OLED */
void showButtons( void );
/*! Display a happy cat */
void happyCat( int );
/*! Display the dead cat image */
void deadCat( int );
/*! Read AN0 and display the pot position */
void testAnalog( void );
/*! Modulate a LED on OC1 with PWM while displaying duty cycle */
void testPWM( void );
/*! Read the TCN75A temperature sensor and display result */
void testTCN75A( void );

/*! Initialization - gets run once */
void setup()
{
  int i;
  
  /* Initialize the OLED */
  oled.begin();
  
  /* Set all LEDs to output */
  /* PIN_IOLED1 through 8 are the 8 LEDs on the Basic I/O Shield */
  /* Note that this only works because the pins are contuguous (70-77) */
  for ( i=PIN_IOLED1; i<=PIN_IOLED8; i++)
    pinMode(i,OUTPUT);
  /* PIN_LED1 is the green LED on the MAX32 (LD4) */
  pinMode(PIN_LED1, OUTPUT);
  /* PIN_LED2 is the yellow LED on the MAX32 (LD5) */
  pinMode(PIN_LED2, OUTPUT);
  
  /* OC1 is a screw terminal */
  pinMode( PIN_IOOC1, OUTPUT );
  
  /* And turn off all LEDs */
  digitalWrite(PIN_LED1, LOW);
  digitalWrite(PIN_LED2, LOW);
  for ( i=PIN_IOLED1; i<=PIN_IOLED8; i++)
    digitalWrite(i, LOW);
  digitalWrite(PIN_IOOC1, LOW);
}

/*! Main program loop - gets called repeatedly */
void loop()
{
  int i,j;
  
  /* Clear the OLED so we can tell when we are testing LEDs */
  oled.clear();
  
  /* Flash the LEDs on the Basic I/O shield */
  for ( i=PIN_IOLED1; i<=PIN_IOLED8; i++)
  {
    digitalWrite(i,HIGH);
    delay(100);
    digitalWrite(i,LOW);
  }
  
  //testBmp();
  //delay(2000);
  
  /* Do a text display and FLASH MAX32 LED2 during the writing time
   * (just to see how long the actual writing takes) */
  digitalWrite(PIN_LED2,HIGH);
  showTextLines();
  digitalWrite(PIN_LED2,LOW);
  /* Now wait a second to view the display */
  delay(1000);

  /* Do a graphical text display and FLASH LED2 during the writing time */
  digitalWrite(PIN_LED2,HIGH);
  showGraphicLines();
  digitalWrite(PIN_LED2,LOW);
  /* Now wait a while to view the display */
  delay(1000);

  /* Show the state of the switches on the Basic I/O board */  
  showSwitches();
  delay(1000);
  
  /* And now the state of the buttons */
  showButtons();
  delay(1000);
  
  /* Well, we gotta do something graphical */
  drawCircle();
  delay(1000);
  
  /* Move the happy cat across the screen  with setPixel() */
  for ( i=105; i>1; i-- )
      happyCat(i);
  delay(1000);
  
  /* Hold loop if SW1 is up */
  while( digitalRead(PIN_IOSW1) )
    ;

/* And shake the dead cat */
  for ( j=0; j<3; j++ )  
  {
    for ( i=40; i<60; i++ )
      deadCat(i);
    for ( i=60; i>40; i-- )
      deadCat(i);
  }
  delay(1000);
  
  /* Hold loop if SW2 is up */
  while( digitalRead(PIN_IOSW2) )
    ;

  /* Modulate LED on OC1 */    
  testPWM();
  
  /* Display the position of the pot */
  testAnalog();
  
  /* Display the temperature */
  testTCN75A();
  
}

unsigned char bump[17] = { 0xff, 0xff, 0x00, 0x00, 0xff, 0xff,  0x00, 0x00, 
  0x00, 0xff, 0xff, 0x00, 0x00, 0xff, 0xff,  0x00, 0x00 };
  
void testBmp( void )
{
 
  /* Clear the display */
  oled.clear();
 
   /* Fill pattern 0 is all black */ 
  oled.setFillPattern(oled.getStdPattern(0));
  
  /* Drawing mode to set pixels to the current color value. */
  oled.setDrawMode(IOShieldOledClass::modeSet);
  
  /* Color 1 is the pixel illuminated. */
  oled.setDrawColor(1);
  
  oled.moveTo(16,4);
  oled.putBmp(8,16,bump);
  oled.updateDisplay();  

}

/*! Draw some text on the left, a circle on the right */
void drawCircle()
{
  double theta;
  double x,y,middlex,middley;
  double radius;
  
  /* Clear the display */
  oled.clear();
 
   /* Fill pattern 0 is all black */ 
  oled.setFillPattern(oled.getStdPattern(0));
  
  /* Drawing mode to set pixels to the current color value. */
  oled.setDrawMode(IOShieldOledClass::modeSet);
  
  /* Color 1 is the pixel illuminated. */
  oled.setDrawColor(1);
  
  /* Draw the text part */
  oled.moveTo(10,12);
  oled.drawString("Circles");
   
  middlex = 128-25;
  middley = 15.5;

  for (radius=15.8; radius>1.9; radius -= 3.7 )
  {
    for ( theta=0.0; theta<6.28; theta += 0.02 )
    {
      x = middlex + radius * sin(theta) + 0.5;
      y = middley + radius * cos(theta) + 0.5;
      oled.moveTo((int)x,(int)y);
      oled.drawPixel();
    }
    oled.updateDisplay();  
  }
}



