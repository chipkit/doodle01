/*! \file test_analog.cpp
 *
 *  \brief Read the A0 pin (pot) and display result
 *
 *
 *  \author jjmcd
 *  \date 2102-12-28
 *
 */
 
/* I/O Shield OLED classes */
#include <IOShieldOled.h>

/* Pin definitions for the Basic I/O Shield */
#include "../basic_io_board.h"

/* Create an instance of the OLED class */
extern IOShieldOledClass oled;

/*! Read AN0 and display the pot position */
void testAnalog( void )
  {
    double analogValue;
    char szWork[16];
    int i;
    
    oled.clear();
    oled.setCursor(0,1);
    oled.putString("Potentiometer");
    oled.setCursor(0,2);
    oled.putString("Position=     .");
    
    for ( i=0; i<1000; i++ )
    {
      analogValue = (double)analogRead( A0 )/ 10.24;
      sprintf(szWork,"%4.1f%%",analogValue);
      oled.setCursor(9,2);
      oled.putString(szWork);
      delay(10);
    }
  }


