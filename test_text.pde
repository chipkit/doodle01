/*! \file test_text.pde
 *
 * \brief Display text and graphics characters on the OLED
 *
 * \author jjmcd
 * \date 2012-12-26
 */

/* I/O Shield OLED classes */
#include <IOShieldOled.h>

/*! Create an instance of the OLED class */
extern IOShieldOledClass oled;



/*! Show four lines of text on the OLED */
void showTextLines( void )
{
  /* Clear the display. */
  oled.clear();

  /* Write some strings to the display. */
  oled.setCursor(0, 0);
  oled.putString("Testing");
  oled.setCursor(3, 1);
  oled.putString("One");
  oled.setCursor(6, 2);
  oled.putString("Two");
  oled.setCursor(9, 3);
  oled.putString("Three");
}

/*! Show four lines of text on the OLED using graphical characters */
void showGraphicLines( void )
{
  /* Clear the display. Note that clear has the same effect as
  ** calling clearBuffer and updateDisplay.
  */  
  oled.clear();

  /* Write some strings to the display.
   *
   * The dot positions result in the first character of each
   * line appearing at the same place as the character
   * positions in showTextLines().
  */
  oled.clearBuffer();
  oled.moveTo(0,0);
  oled.drawString("Testing");
  oled.moveTo(24,8);
  oled.drawString("Un");
  oled.moveTo(48,16);
  oled.drawString("Deux");
  oled.moveTo(72,24);
  oled.drawString("Trois");
  oled.updateDisplay();  
}



